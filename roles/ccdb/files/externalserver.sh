#!/bin/bash
WILDFLY_DIR="/opt/wildfly-8.2.0.Final"
TEXT="                <host name=\"cable\" alias=\"localhost\">"
if grep -Fxq "$TEXT" $WILDFLY_DIR/standalone/configuration/standalone.xml
then
    echo "exists!"   
else
    echo "Does not exist"
    sed -i 's/<server name=\"default-server_external\">/<server name=\"default-server_external\">\n                <https-listener name=\"default_external\" socket-binding=\"https_external\" security-realm=\"UndertowRealm\"\/>\n                <host name=\"default-host\" alias=\"localhost\">\n                    <location name=\"\/\" handler=\"welcome-content\"\/>\n                    <filter-ref name=\"server-header\"\/>\n                    <filter-ref name=\"x-powered-by-header\"\/>\n                <\/host>\n                <host name=\"cable\" alias=\"localhost\">\n                    <filter-ref name=\"server-header\"\/>\n                    <filter-ref name=\"x-powered-by-header\"\/>\n                <\/host>/' $WILDFLY_DIR/standalone/configuration/standalone.xml
fi
