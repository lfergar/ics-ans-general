#!/bin/bash
WILDFLY_USER="yildflwadm"
WILDFLY_PASSWORD="yildflwadmpass!"
WILDFLY_DIR="/opt/wildfly-8.2.0.Final"
TEXT="                        rbac"
if grep -Fxq "$TEXT" $WILDFLY_DIR/standalone/configuration/standalone.xml
then
    echo "exists!"   
else
    echo "Does not exist"
    sed -i 's/<xa-datasource jndi-name=\"java:\/rbac\" pool-name=\"RbacDS\" enabled=\"true\" use-ccm=\"false\">/<xa-datasource jndi-name=\"java:\/rbac\" pool-name=\"RbacDS\" enabled=\"true\" use-ccm=\"false\">\n                    <xa-datasource-property name=\"DatabaseName\">\n                        rbac\n                    <\/xa-datasource-property>\n                    <xa-datasource-property name=\"PortNumber\">\n                        5432\n                    <\/xa-datasource-property>\n                    <xa-datasource-property name=\"ServerName\">\n                        localhost\n                    <\/xa-datasource-property>\n                    <xa-datasource-class>org.postgresql.xa.PGXADataSource<\/xa-datasource-class>\n                    <driver>postgresql<\/driver>\n                    <xa-pool>\n                        <is-same-rm-override>false<\/is-same-rm-override>\n                        <interleaving>false<\/interleaving>\n                        <pad-xid>false<\/pad-xid>\n                        <wrap-xa-resource>false<\/wrap-xa-resource>\n                    <\/xa-pool>\n                    <security>\n                        <user-name>'"$WILDFLY_USER"'<\/user-name>\n                        <password>'"$WILDFLY_PASSWORD"'<\/password>\n                    <\/security>\n                    <validation>\n                        <validate-on-match>false<\/validate-on-match>\n                        <background-validation>false<\/background-validation>\n                    <\/validation>\n                    <statement>\n                        <share-prepared-statements>false<\/share-prepared-statements>\n                    <\/statement>\n                <\/xa-datasource>/' $WILDFLY_DIR/standalone/configuration/standalone.xml
fi
