#!/bin/bash
#############################################################
# Configuration
#############################################################
# PostgreSQL User With Backup Permission
export PG_BACKUP_USER=@PSQL_BACKUP_USER@

# Password for the backup user of PostgreSQL
export PGPASSWORD=@PSQL_BACKUP_PASSWORD@

# Directory where backups will be saved
BACKUPS_DIR=@PSQL_BACKUP_FOLDER@

# Logfilename for standard output
LOGFILE=$BACKUPS_DIR/log.txt

# Maximum number of backup directories
# Over that number the oldest backup will be deleted prior invoking pg_basebackup
MAXIMUM_BACKUP_DIRS=100
#############################################################

# Make sure the directory exists
mkdir -p $BACKUPS_DIR


# Check if the number of directories is too big, and delete the oldest one
NUM_OF_DIRS=`echo $BACKUPS_DIR/backup*/ | wc -w`
if [ "$NUM_OF_DIRS" -gt "$MAXIMUM_BACKUP_DIRS" ]
then
    # Delete the oldest directory
    DIR_TO_DELETE=`ls -lt $BACKUPS_DIR | grep '^d' | tail -1 | cut -d' ' -f9`

    echo "More than $NUM_OF_DIRS files in the backup directory. Deleting $DIR_TO_DELETE.." >>$LOGFILE
    rm -R $BACKUPS_DIR/$DIR_TO_DELETE
fi

# Do the backup
pg_basebackup -U $PG_BACKUP_USER -Ft -D $BACKUPS_DIR/backup-`date '+%Y-%m-%d'` -Xf -z >>$LOGFILE
